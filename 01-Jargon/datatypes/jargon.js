/*
 * JARGON
 *
 *   * Data Types
 *      * Strings
 *      * Numbers
 *      * Boolean
 *      * Objects
 *          * Arrays
 *          * Functions
 *
 */

var a = "I am a string of text.";
var b = 100;

var stringAndNum = a + b;

//console.log(stringAndNum);
//console.log(typeof stringAndNum);

var c = true;

var d = {
    name: "bob",
    age: 50
};

var e = [1,4,10];
var f = ["apple", "orange", "banana"];