//// BASICS
var name = "Bob";
var age = 50;

function tellAPersonsAge(name, age) {
    $("#age-statement").append(name + " is " + age + " years old");
}

/*function tellAPersonsAge(name="Michael", age = "many") {
    $("#age-statement").append(name + " is " + age + " years old. ");
}*/

tellAPersonsAge("Greg", 15);
//tellAPersonsAge(name,age);
//tellAPersonsAge();

var increaseAge = function(age) {
    age++;
    alert(age);
};

//increaseAge(50);


//// SCOPE

var name = "George";

function sayMyName() {
    var prefix = "My name is: ";
    var sentence = prefix + name;
    alert(sentence);
}

sayMyName();
alert(sentence); //this will not work. sentence is within the scope of the function.