var Bob = {
    name: "Bob",
    age: 50,
    speak: function(){
        alert("Howdy, I'm " + this.name);
    }
};

/*
var Bob = function() {
    this.name = "Bob";
    this.age = 50;
    this.speak = function() {
        alert("Howdy, I'm " + this.name);
    }
};*/


/*
var Cat = function(name, age) {
  this.name = name;
  this.age = age;
  this.speak = function() {
      alert("Meow, I'm " + this.name + ". I'm " + this.age + " years old");
      return this;
  };
  this.increaseAge = function() {
      this.age++;
      return this;
  }
};*/
