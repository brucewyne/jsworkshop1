var StreetFighter = function(gameElement) {

    this.$gameElement = gameElement;
    this.$punchButton = this.$gameElement.find(".button-punch");
    this.$specialButton = this.$gameElement.find(".button-special");
    this.$playerHealthMeter = this.$gameElement.find('.player-health');
    this.$opponentHealthMeter = this.$gameElement.find('.opponent-health');
    this.$log = this.$gameElement.find(".status");
    this.playerHealth = 100;
    this.opponentHealth = 100;
    this.opponentAttack = 10;
    this.playerAttack = 7;
    this.playerSpecialAttack = 12;
    this.winner = "";

    var self = this;


    this.start = function() {
        this.$punchButton.click(function(){
            self.punch();
        });
        this.$specialButton.click(function(){
            self.specialAttack();
        });
    };

    this.end = function() {
        this.$punchButton.off();
        this.$specialButton.off();
        if (this.winner == "Opponent") {
            alert("Game over! You lose!");
        } else {
            alert("Game over! You win!");
        }
    };

    this.punch = function() {
        this.opponentHealth = this.opponentHealth - this.playerAttack;
        this.logTurn("Player", "Punch", this.playerAttack);
        this.endPlayerTurn();
    };

    this.specialAttack = function() {
        this.opponentHealth = this.opponentHealth - this.playerSpecialAttack;
        this.logTurn("Player", "Special Attack", this.playerSpecialAttack);
        this.endPlayerTurn();
    };

    this.endPlayerTurn = function() {
        this.updateMeters();
        this.checkForVictory();
        this.opponentTurn();
    };

    this.opponentTurn = function() {
        self.playerHealth = self.playerHealth - self.opponentAttack;
        self.updateMeters();
        self.logTurn("Opponent", "Attack", self.opponentAttack);
        this.checkForVictory();
    };

    this.checkForVictory = function(){
        if (this.playerHealth <= 0) {
            this.winner = "Opponent";
            this.end();
        }
        if (this.opponentHealth <= 0) {
            this.winner = "Player";
            this.end();
        }
    };

    this.updateMeters = function() {
        this.$playerHealthMeter.width(this.playerHealth + "%")
        this.$opponentHealthMeter.width(this.opponentHealth + "%");
    };

    this.logTurn = function(player, attack, hitpoints) {
        this.$log.append("<li>" + player + " used " + attack + " and did " + hitpoints + " damage.");
    }

};

var MyGame = new StreetFighter($("#street-fight-game"));
MyGame.start();