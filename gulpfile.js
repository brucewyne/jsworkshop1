var gulp = require("gulp"),
    pathPrefix = "",
    sass = require("gulp-sass"),
    sourcemaps = require("gulp-sourcemaps"),
    autoprefixer = require("gulp-autoprefixer"),
    server = require('gulp-server-livereload'),
    scssPath = pathPrefix + "./assets/scss",
    outputPath = pathPrefix + "./assets/css/",
    filesToWatch = [scssPath + "/**/*.scss"],
    fileToCompile = scssPath + '/main.scss';

gulp.task('process-sass', function() {
    return gulp.src(fileToCompile)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compact',includePaths:['./node_modules/foundation-sites/scss']}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(autoprefixer())
        .pipe(gulp.dest(outputPath));
});

gulp.task('webserver', function() {
    gulp.src('./')
        .pipe(server({
            livereload: true,
            directoryListing: true,
            open: true
        }));
});

gulp.task('watch', function() {
    gulp.watch(filesToWatch, gulp.series('process-sass'));
});

//for dev/local
gulp.task('default', gulp.series('process-sass', 'webserver', 'watch'));